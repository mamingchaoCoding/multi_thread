package com.mamingchao.concurrent.fromVectorToQueue;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 *
 * Created by mamingchao on 2020/11/4.
 */
public class TicketSeller3 {
    static Queue<Ticket> pool = new ConcurrentLinkedQueue<>();

    static {
        for (int i = 0; i < 1000; i++) {
            pool.add(new Ticket("票号--" + i));
        }
    }

    public static void main(String[] args) {

        for (int i = 0; i <10; i++) {
            new Thread(()->{
                while (pool.size()>0) {

                    System.out.println("窗口售票--" + pool.poll() );
                }
            }).start();

        }

//        for (int i = 0; i < threads.length; i++) {
//            threads[i].start();
//            try {
//                threads[i].join();
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//        }

//        System.out.println("剩余的火车票--" + pool.size());
    }

    static class Ticket{
        String ticketNo;

        public Ticket(String ticketNo) {
            this.ticketNo = ticketNo;
        }

        @Override
        public String toString() {
            return "Ticket{" +
                    "ticketNo='" + ticketNo + '\'' +
                    '}';
        }
    }
}
