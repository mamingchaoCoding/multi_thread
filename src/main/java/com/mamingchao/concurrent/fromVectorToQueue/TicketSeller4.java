package com.mamingchao.concurrent.fromVectorToQueue;

import java.util.Vector;
import java.util.concurrent.TimeUnit;

/**
 * 这个实现，也会出现超卖的现象，
 * 虽然 size()方法和 remove()方法 每个单独拿出来 都是 synchronized，但是
 * Thread runnable 里面 的 方法块，却不是 原子操作
 *
 * Created by mamingchao on 2020/11/4.
 */
public class TicketSeller4 {
    static Vector<Ticket> pool = new Vector<>();

    static {
        for (int i = 0; i < 1000; i++) {
            pool.add(new Ticket("票号--" + i));
        }
    }

    public static void main(String[] args) {

        for (int i = 0; i <10; i++) {
            new Thread(()->{
                synchronized (pool){
                    while (pool.size()>0) {

                        try {
                            TimeUnit.MILLISECONDS.sleep(10);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        System.out.println("窗口售票--" + pool.remove(0) );
                    }
                }
            }).start();

        }

//        for (int i = 0; i < threads.length; i++) {
//            threads[i].start();
//            try {
//                threads[i].join();
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//        }

//        System.out.println("剩余的火车票--" + pool.size());
    }

    static class Ticket{
        String ticketNo;

        public Ticket(String ticketNo) {
            this.ticketNo = ticketNo;
        }

        @Override
        public String toString() {
            return "Ticket{" +
                    "ticketNo='" + ticketNo + '\'' +
                    '}';
        }
    }
}
