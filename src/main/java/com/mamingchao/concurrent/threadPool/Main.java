package com.mamingchao.concurrent.threadPool;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by mamingchao on 2020/11/9.
 */
public class Main {

    public static void main(String[] args) {
        ExecutorService service = Executors.newCachedThreadPool();
        service.submit(new Runnable() {
            @Override
            public void run() {

            }
        });

        System.out.println(Integer.MAX_VALUE);

    }
}
