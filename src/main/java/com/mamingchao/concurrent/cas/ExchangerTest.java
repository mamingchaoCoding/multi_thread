package com.mamingchao.concurrent.cas;

import java.util.concurrent.Exchanger;
import java.util.concurrent.TimeUnit;

/**
 * Created by mamingchao on 2020/7/16.
 */
public class ExchangerTest {

    static Exchanger<String> exchanger = new Exchanger<>();


    public static void main(String[] args) {
        new Thread(() -> {
            String s = "T1";
            try {
                s = exchanger.exchange(s);
                System.out.println("T1 交换后的结果 " + s);
            } catch(InterruptedException e) {
                e.printStackTrace();
            }
        }, "T1").start();

        new Thread(() -> {
            String s = "T2";
            try {
                TimeUnit.SECONDS.sleep(2);
                s = exchanger.exchange(s);
                System.out.println("T2 交换后的结果 " + s);
            } catch(InterruptedException e) {
                e.printStackTrace();
            }
        }, "T2").start();
    }

}
