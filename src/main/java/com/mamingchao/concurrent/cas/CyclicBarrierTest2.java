package com.mamingchao.concurrent.cas;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

/**
 * Created by mamingchao on 2020/7/8.
 */
public class CyclicBarrierTest2 {
    static CyclicBarrier barrier = new CyclicBarrier(5, new Runnable() {
        @Override
        public void run() {
            System.out.println("满人，发车");
        }
    });

    public static void main(String[] args) {

        for (int j=0;j<20;j++) {
            new Thread(() -> {
                try {
                    barrier.await();

                }catch (InterruptedException e) {
                    e.printStackTrace();
                }catch (BrokenBarrierException e) {
                    e.printStackTrace();
                }
            }).start();
        }
    }
}
