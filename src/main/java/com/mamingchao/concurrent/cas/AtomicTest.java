package com.mamingchao.concurrent.cas;

import java.util.concurrent.atomic.AtomicLong;

/**
 * Atomic 比synchronized 快 是因为前者是cas 无锁操作
 * 后者是锁，所以前者快
 * Created by mamingchao on 2020/7/6.
 */
public class AtomicTest {
    static AtomicLong count1 = new AtomicLong(0);

    public static void main(String[] args) throws Exception{
        Thread[] threads = new Thread[1000];

        for (int i=0;i< threads.length;i++) {
            threads[i] = new Thread(()-> {
                for (int k=0;k< 100000;k ++)
                    count1.incrementAndGet();
            });
        }

        Long start = System.currentTimeMillis();

        

        for (int i=0;i< threads.length;i++) {
            threads[i].start();
        }

        for (int i=0;i< threads.length;i++) {
            threads[i].join();
        }

        Long end = System.currentTimeMillis();

        System.out.println("Atomic long time cost-" + (end - start));
    }
}
