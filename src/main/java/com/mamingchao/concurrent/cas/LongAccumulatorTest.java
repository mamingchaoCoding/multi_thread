package com.mamingchao.concurrent.cas;//package com.juc.mamingchao.cas;
//
//import java.util.concurrent.atomic.AtomicLong;
//import java.util.concurrent.atomic.LongAccumulator;
//
///**
// * Created by mamingchao on 2020/7/6.
// */
//public class LongAccumulatorTest {
//    static LongAccumulator count1 = new LongAccumulator();
//
//    public static void main(String[] args) throws Exception{
//        Thread[] threads = new Thread[1000];
//
//        for (int i=0;i< threads.length;i++) {
//            threads[i] = new Thread(()-> {
//                for (int k=0;k< 100000;k ++)
//                    count1.incrementAndGet();
//            });
//        }
//
//        Long start = System.currentTimeMillis();
//
//        for (int i=0;i< threads.length;i++) {
//            threads[i].start();
//        }
//
//        for (int i=0;i< threads.length;i++) {
//            threads[i].join();
//        }
//
//        Long end = System.currentTimeMillis();
//
//        System.out.println("Atomic long time cost-" + (end - start));
//    }
//}
