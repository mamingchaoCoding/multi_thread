package com.mamingchao.concurrent.cas;

import java.util.concurrent.atomic.LongAdder;

/**
 * LongAdder 比Atomic 快是因为 前者是分段锁
 * 分段锁也是基于CAS的
 * Created by mamingchao on 2020/7/6.
 */
public class LongAdderTest {
    static LongAdder count1 = new LongAdder();

    public static void main(String[] args) throws Exception{
        Thread[] threads = new Thread[1000];

        for (int i=0;i< threads.length;i++) {
            threads[i] = new Thread(()-> {
                for (int k=0;k< 100000;k ++)
                    count1.increment();
            });
        }

        Long start = System.currentTimeMillis();

        for (int i=0;i< threads.length;i++) {
            threads[i].start();
        }

        for (int i=0;i< threads.length;i++) {
            threads[i].join();
        }

        Long end = System.currentTimeMillis();

        System.out.println("Atomic long time cost-" + (end - start));
    }
}
