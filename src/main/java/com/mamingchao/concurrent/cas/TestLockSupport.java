package com.mamingchao.concurrent.cas;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.LockSupport;

/**
 * 在没有LockSupport 之前，如果想让某个线程 阻塞住，需要用锁的wait(),或者 await()
 * 锁需要锁到某个对象上
 * 叫醒这个线程 需要 notify()
 * 原来到线程 等待，是等待在队列里，如果这个thread 不是first in ,想要叫醒某个等待的队列是非常费劲的
 *
 * LockSupport不需要，随时停park(),随时继续走unpark(),可以unpart(t) 指定线程;
 *
 *LockSupport 是通过Unsafe.park()方法实现，Unsafe 是c和c++实现的，这里看不到源码
 *
 * Created by mamingchao on 2020/9/19.
 */
public class TestLockSupport {

    public static void main(String[] args) throws InterruptedException {
        Thread t = new Thread(() -> {
            for (int i = 0; i < 10 ; i++) {
                System.out.println(i);
                if (i == 5)
                    LockSupport.park();

                //如果这里多park一次，那么到5的时候，还是会park住
                //park()和unpark()需要一对一对应上，他们 实现 是通过计数器
                if (i == 3)
                    LockSupport.park();
                try {
                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        t.start();

        //LockSupport 可以先unpark(),相当于先放行，再park();这样到park()的时候，直接就过去
        //因为这里是主线程，下面这个sleep 如果注视掉，上面的线程和主线程并行执行，这里的unpark是先执行的
//        TimeUnit.SECONDS.sleep(8);

        LockSupport.unpark(t);

        System.out.println("Thread t, you can going on!");

    }
}
