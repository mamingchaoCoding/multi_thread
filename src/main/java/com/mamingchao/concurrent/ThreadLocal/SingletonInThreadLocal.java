package com.mamingchao.concurrent.ThreadLocal;

import java.util.concurrent.TimeUnit;

/**
 * 这是2020年12月14号面试 富达 的题，我成功的掉到坑里面了
 * 先问最熟悉的设计模式，我回答是单例
 * 那么对于单例，在多线程的情况下，怎么处理
 *
 * 单例肯定是线程不安全啊，需要加锁啊
 * 我竟然回答 ThreadLocal 。。。
 *
 * 所以，ThreadLocal 只是 每个Thread里有一个自己线程的map，key 是ThreadLocal对象，然后value是放进去的值
 * 如果 value是个单例，那么 其他线程 只获取了这个单例，就拿到这个对象了，而不用去 你 ThreadLocalMap里面的值
 * Created by mamingchao on 2020/11/3.
 */
public class SingletonInThreadLocal {

    static ThreadLocal<Person> personLocal = new ThreadLocal<>();

    public static void main(String[] args) {
        Thread t1 = new Thread(()->{
            Person person = Person.getSingletonPerson();
            person.setName("zhangsan");
            personLocal.set(person);

            System.out.println("Thread 1 " +  personLocal.get());

            try {
                TimeUnit.SECONDS.sleep(5);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Thread 1 " +  personLocal.get());

        });

        Thread t2 = new Thread(()->{
            Person person = Person.getSingletonPerson();
            person.setName("lisi");
            personLocal.set(person);


            System.out.println("Thread 2 " + personLocal.get());

        });

        t1.start();
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        t2.start();

//        System.out.println(personLocal.get());
    }



    static public class Person{
        static Person instance = new Person();
        String name;

        private Person() {

        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public static Person getSingletonPerson() {
            return instance;
        }

        @Override
        public String toString() {
            return "Person{" +
                    "name='" + name + '\'' +
                    '}';
        }
    }
}
