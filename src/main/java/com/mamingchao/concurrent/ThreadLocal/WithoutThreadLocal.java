package com.mamingchao.concurrent.ThreadLocal;

import java.util.concurrent.TimeUnit;

/**
 * Created by mamingchao on 2020/11/3.
 */
public class WithoutThreadLocal {

    static Person p = new Person();


    public static void main(String[] args) {
        new Thread(()->{
            System.out.println(p.name);

            try {
                TimeUnit.SECONDS.sleep(2);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            System.out.println(p.name);

        }).start();

        new Thread(()->{

            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            p.name = "lisi";

        }).start();
    }


    static private class Person{
        String name = "zhangsan";
    }
}
