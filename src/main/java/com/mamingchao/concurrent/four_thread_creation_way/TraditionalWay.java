package com.mamingchao.concurrent.four_thread_creation_way;

/**
 * Created by mamingchao on 2020/9/22.
 */
public class TraditionalWay {

    public static void main(String[] args) {
        new Thread(new Runnable() {

            @Override
            public void run() {
                char a = 'A';
                for (int i = 0; i <26; i++) {
                    System.out.println(a);
                    a++;
                }
            }
        },"thread1").start();
    }
}
