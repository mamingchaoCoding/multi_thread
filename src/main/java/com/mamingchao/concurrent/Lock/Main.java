package com.mamingchao.concurrent.Lock;

/**
 * Created by mamingchao on 2020/10/26.
 */
public class Main {

    public static void main(String[] args) {

        Thread t = new Thread(()->{
            System.out.println("This is a daemon thread!");
        });
        t.setDaemon(true);
        t.start();
    }
}
