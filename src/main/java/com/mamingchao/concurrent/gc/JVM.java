package com.mamingchao.concurrent.gc;

import java.util.HashSet;

/**
 * Created by mamingchao on 2020/6/29.
 */
public class JVM {
    public static void main(String[] args) throws Exception {

        HashSet set = new HashSet();

        Object  o = new Object();

        //用list保持着引用 防止full gc回收常量池
//        List<String> list = new ArrayList<String>();
//        int i = 0;
//        while(true){
//            list.add(String.valueOf(i++).intern());
//        }

//        int sum = 0;
//        int count = 1000000;
//        //warm up
//        for (int i = 0; i < count ; i++) {
//            sum += fn(i);
//        }
//
//        Thread.sleep(500);
//
//        for (int i = 0; i < count ; i++) {
//            sum += fn(i);
//        }
//
//        System.out.println(sum);
//        System.in.read();
    }

    private static int fn(int age) {
        User user = new User(age);
        int i = user.getAge();
        return i;
    }
}

class User {
    private final int age;

    public User(int age) {
        this.age = age;
    }

    public int getAge() {
        return age;
    }
}
