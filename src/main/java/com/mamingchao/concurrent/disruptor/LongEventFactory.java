package com.mamingchao.concurrent.disruptor;

import com.lmax.disruptor.EventFactory;

/**
 * Created by mamingchao on 2021/1/6.
 */
public class LongEventFactory implements EventFactory<LongEvent> {


    @Override
    public LongEvent newInstance() {
        return new LongEvent();
    }
}
