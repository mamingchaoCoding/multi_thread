package com.mamingchao.concurrent.disruptor;

import com.lmax.disruptor.EventHandler;

/**
 * Created by mamingchao on 2021/1/6.
 */
public class LongEventHandler implements EventHandler<LongEvent> {

    @Override
    public void onEvent(LongEvent longEvent, long l, boolean b) throws Exception {
        System.out.printf("Current event is %s" , longEvent);
        System.out.println();
    }
}
