package com.mamingchao.concurrent.disruptor;

/**
 * Created by mamingchao on 2021/1/6.
 */
public class LongEvent{
    private long value;

    public void set(long value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Event{" +
                "value=" + value +
                '}';
    }
}
