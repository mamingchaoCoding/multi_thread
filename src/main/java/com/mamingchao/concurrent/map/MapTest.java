package com.mamingchao.concurrent.map;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentSkipListMap;

/**
 * 测试一下 这几个map的性能，读写
 * {@link HashMap} 无序、非线程安全的map，基于链表实现
 * {@link ConcurrentHashMap} 无序、线程安全的map，基于HashMap 和cas实现
 *
 * {@link TreeMap} 有序、非线程安全的map，基于红黑树 实现
 *
 * {@link ConcurrentHashMap} 无序、线程安全的map，基于HashMap 和cas实现
 *
 * Created by mamingchao on 2020/11/8.
 */
public class MapTest {
    static Map<String,String> map = new HashMap();
    static Map<String,String> map1 = new ConcurrentHashMap();

    static Map<String,String> map2 = new TreeMap();
    static Map<String,String> map3= new ConcurrentSkipListMap<>();

    public static void main(String[] args) {
        map2.put("mabeijiang","father");
        map2.put("mamingchao","son");
        map2.put("mayuzhuo","grandson");

    }


}

