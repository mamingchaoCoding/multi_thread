package com.mamingchao.concurrent.FourReference;

import java.io.IOException;

/**
 * 我们平时使用的正常引用就是强引用
 * 被强引用的对象，只要又引用指向这个对象不会被回收
 * Created by mamingchao on 2020/11/3.
 */
public class NormalReference {
    public static void main(String[] args) throws IOException {
        M m = new M();

        System.gc();
        System.out.println(m.hashCode());

        m=null;
        System.gc();

        //这句的意思，主要是让主线程阻塞住；如果主线程停了，gc 不gc的没用了，可能不需要了gc
        //因为 垃圾回收是另外的线程做的
        System.in.read();
    }
}
