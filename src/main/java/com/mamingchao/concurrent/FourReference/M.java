package com.mamingchao.concurrent.FourReference;

/**
 * 每个对象被回收的时候，都会调用 Object 父类里的finalize
 * 这里重写一下，打印一句话，方便我们 了解，对象是不是真的被回收了
 * Created by mamingchao on 2020/11/3.
 */
public class M {

    @Override
    protected void finalize() throws Throwable {
        System.out.println("finalize");
        super.finalize();
    }
}
