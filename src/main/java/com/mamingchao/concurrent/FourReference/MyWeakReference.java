package com.mamingchao.concurrent.FourReference;

import java.lang.ref.WeakReference;

/**
 * 弱引用，只要有gc，就一定会被回收
 *
 * Created by mamingchao on 2020/11/3.
 */
public class MyWeakReference {
    public static void main(String[] args) {
        WeakReference<M> m  = new WeakReference<>(new M());

        System.out.println(m.hashCode());
        System.gc();
        System.out.println(m.hashCode());

//        WeakHashMap map = new WeakHashMap();
//        map.put()

    }
}
