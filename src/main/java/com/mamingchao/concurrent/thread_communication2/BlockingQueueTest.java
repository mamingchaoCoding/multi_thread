package com.mamingchao.concurrent.thread_communication2;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

/**
 * Created by mamingchao on 2020/9/23.
 */
public class BlockingQueueTest {

    private static BlockingQueue<String> blockingQueue = new ArrayBlockingQueue<String>(4);

    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {
            String threadName = "put thread--" + i;
            String value = i + "";
            new Thread(()->{
                try {
                    blockingQueue.put(value);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(threadName + "  has put in " + value);
            }).start();
        }

        for (int i = 0; i < 20; i++) {
            String threadName = "get thread--" + i;
            new Thread(()->{
                Object v = null;
                try {
                    v = blockingQueue.take();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(threadName + " has get " + v);
            }).start();
        }
    }


}
