package com.mamingchao.concurrent.threads_communication3;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.LockSupport;

/**
 * 这个也有问题,当没有sleep的时候，个别、偶然情况下，结果不对
 *
 * 这个跟 CountDownLatch是一样的，都是a通知b unpark的时候，a继续向下走了
 * 结果对不对，就看a和b谁跑的快了，所以结果不准确
 *
 * 这里解决这个问题的办法也跟CountDownLatch 一样的，用两个 LockSupport
 * Created by mamingchao on 2020/9/19.
 */
public class WithLockSupport {

    static List<Integer> lists = new ArrayList<>();

    static Thread a = null,b=null;

    public static void main(String[] args) {

        b = new Thread( ()->{
            LockSupport.park();
            System.out.println("线程B收到通知，开始执行自己的业务...");
            LockSupport.unpark(a);

        });

        a = new Thread(() -> {

            for (int i = 0; i < 10; i++) {
                lists.add(i);
                System.out.println(i);
//                try {
//                    TimeUnit.SECONDS.sleep(1);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
                if (i == 5){
                    LockSupport.unpark(b);
                    LockSupport.park();
                }
            }
        });


        b.start();
//        try {
//                TimeUnit.SECONDS.sleep(1);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
        a.start();
    }
}
